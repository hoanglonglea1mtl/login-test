import 'package:get/get.dart';
import 'package:login_test/view_models/view_models.dart';
import 'package:login_test/views/login/view.dart';
import 'app_routes.dart';

abstract class AppPages {
  static final List<GetPage<dynamic>> pages = [
    GetPage(
      name: Routes.login,
      page: () =>  LoginView(),
      bindings: [
        BindingsBuilder.put(() => LoginViewModel()),
      ],
    ),
  ];
}
