import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'data/data.dart';
import 'routes/routes.dart';
import 'theme/theme.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    EasyLoading.instance
      ..displayDuration = const Duration(milliseconds: 500)
      ..errorWidget = const Icon(
        Icons.clear,
        color: Colors.red,
        size: 50,
      )
      ..successWidget = const Icon(
        Icons.done,
        color: Colors.green,
        size: 50,
      )
      ..indicatorType = EasyLoadingIndicatorType.ring
      ..loadingStyle = EasyLoadingStyle.custom
      ..indicatorSize = 45.0
      ..radius = 10.0
      ..backgroundColor = Colors.transparent
      ..indicatorColor = AppColors.primary
      ..textColor = Colors.white
      ..maskType = EasyLoadingMaskType.black
      ..maskColor = AppColors.primary.withOpacity(0.5)
      ..userInteractions = true
      ..dismissOnTap = false;
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      getPages: AppPages.pages,
      theme: AirTalkThemeData.themeData,
      darkTheme: AirTalkThemeData.themeData,
      builder: EasyLoading.init(builder: _builder),
      defaultTransition: Transition.noTransition,
      initialRoute: Routes.login,
      initialBinding: AppBindings(),
    );
  }

  Widget _builder(BuildContext context, Widget? child) {
    const textScale = 1.0;
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: textScale),
      child: child ?? const SizedBox(),
    );
  }
}

class AppBindings extends Bindings {
  @override
  void dependencies() {
    // Get.put(AppService());
    Get.lazyPut<IAuthRepository>(() => AuthRepository(), fenix: true);
  }
}
