import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import '../models/models.dart';

const pageSizeResult = 20;

class AppService extends BaseService {
  AppService() {
    initAppService();
  }

  Future<BaseResponseModel> get(String path,
      {Map<String, dynamic>? queryParameters,
      Function(String)? customErrorRes,
      Function(DioError)? customErrorCatch}) async {
    try {
      debugPrint('---PATH---\nGET:$path');
      debugPrint('---PARAMS---\n:${queryParameters ?? ''}');
      final res = await _dio.get(path, queryParameters: queryParameters);
      debugPrint('---RESPONSE-$path--:\n$res');
      if (isSuccess(res)) {
        return BaseResponseModel(isSuccess: true, data: responseData(res));
      } else {
        EasyLoading.dismiss();
        if (customErrorRes != null) {
          customErrorRes.call(getMessage(res));
        } else {
          EasyLoading.showError(getMessage(res));
        }
      }
    } catch (e) {
      if (customErrorCatch != null) {
        customErrorCatch.call(e as DioError);
      } else {
        checkError(e as DioError);
      }
    }
    return BaseResponseModel(isSuccess: false, data: null);
  }

  Future<BaseResponseModel> post(String path,
      {Map<String, dynamic>? queryParameters,
      dynamic data,
      Function(String)? customErrorRes,
      Function(DioError)? customErrorCatch,
      bool isCreate = false}) async {
    try {
      debugPrint('---PATH---\nPOST:$path');
      debugPrint('---PARAMS---\n:${queryParameters ?? ''}');
      debugPrint('---DATA---\n:${data ?? ''}');
      final res = await _dio.post(path, queryParameters: queryParameters, data: data);
      debugPrint('---RESPONSE---\n$res');
      if (isSuccess(res, isCreate: isCreate)) {
        return BaseResponseModel(isSuccess: true, data: responseData(res));
      } else {
        EasyLoading.dismiss();
        if (customErrorRes != null) {
          customErrorRes.call(getMessage(res));
        } else {
          EasyLoading.showError(getMessage(res));
        }
      }
    } catch (e) {
      debugPrint('------Error:\n${e.toString()}');
      if (customErrorCatch != null) {
        customErrorCatch.call(e as DioError);
      } else {
        checkError(e as DioError);
      }
    }
    return BaseResponseModel(isSuccess: false, data: null);
  }

  Future<BaseResponseModel> put(String path,
      {Map<String, dynamic>? queryParameters, dynamic data}) async {
    try {
      debugPrint('---PATH---\nPUT:$path');
      debugPrint('---PARAMS---\n:${queryParameters ?? ''}');
      debugPrint('---DATA---\n:${data ?? ''}');
      final res = await _dio.put(path, queryParameters: queryParameters, data: data);
      debugPrint('---RESPONSE---\n$res');
      if (isSuccess(res)) {
        return BaseResponseModel(isSuccess: true, data: responseData(res));
      } else {
        EasyLoading.dismiss();
        EasyLoading.showError(getMessage(res));
      }
    } catch (e) {
      checkError(e as DioError);
    }
    return BaseResponseModel(isSuccess: false, data: null);
  }

  Future<BaseResponseModel> patch(String path,
      {Map<String, dynamic>? queryParameters, dynamic data}) async {
    try {
      debugPrint('---PATH---\nPATCH:$path');
      debugPrint('---PARAMS---\n:${queryParameters ?? ''}');
      debugPrint('---DATA---\n:${data ?? ''}');
      final res = await _dio.patch(path, queryParameters: queryParameters, data: data);
      debugPrint('---RESPONSE---\n$res');
      if (isSuccess(res)) {
        return BaseResponseModel(isSuccess: true, data: responseData(res));
      } else {
        EasyLoading.dismiss();
        EasyLoading.showError(getMessage(res));
      }
    } catch (e) {
      checkError(e as DioError);
    }
    return BaseResponseModel(isSuccess: false, data: null);
  }

  Future<BaseResponseModel> delete(String path,
      {Map<String, dynamic>? queryParameters, dynamic data}) async {
    try {
      debugPrint('---PATH---\nDELETE:$path');
      debugPrint('---PARAMS---\n:${queryParameters ?? ''}');
      debugPrint('---DATA---\n:${data ?? ''}');
      final res = await _dio.delete(path, queryParameters: queryParameters, data: data);
      debugPrint('---RESPONSE---\n$res');
      if (isSuccess(res)) {
        return BaseResponseModel(isSuccess: true, data: responseData(res));
      } else {
        EasyLoading.dismiss();
        EasyLoading.showError(getMessage(res));
      }
    } catch (e) {
      checkError(e as DioError);
    }
    return BaseResponseModel(isSuccess: false, data: null);
  }
}

abstract class BaseService {
  late Dio _dio;
  String? _token;

  void initAppService() {
    final headers = <String, dynamic>{
      'content-type': 'application/json',
      'accept': 'application/json',
      'Authorization': 'Bearer $_token',
    };
    debugPrint('---Header: $headers');
    _dio = Dio(
      BaseOptions(
        baseUrl: 'https://domain/api',
        connectTimeout: 15000,
        receiveTimeout: 15000,
        responseType: ResponseType.json,
        headers: headers,
      ),
    );
  }

  void setToken(String? token) {
    _token = token;
    initAppService();
  }

  bool isSuccess(Response response, {bool isCreate = false}) {
    return response.data['status']==200;
  }

  String getMessage(Response response) {
    return response.data['message'];
  }

  dynamic responseData(Response response) {
    try {
      final a = response.data['data'];
      if (a != null) {
        return a;
      }
      return response.data;
    } catch (e) {
      return e.toString();
    }
  }

  void checkError(DioError e) {
    EasyLoading.dismiss();
    debugPrint('---ERROR---\n${e.toString()}');
    if (e.response?.statusCode == 401) {
      //Get.find<AppController>().logout();
      EasyLoading.showError('An error occurred!');
    } else {
      try {
        EasyLoading.showError(e.response!.data['message']);
      } catch (E) {
        EasyLoading.showError('An error occurred!');
      }
    }
  }
}
