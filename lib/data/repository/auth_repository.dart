import 'package:get/get.dart';

abstract class IAuthRepository {
  Future<bool> login({required String email, required String password});
}

class AuthRepository implements IAuthRepository {
  //AppService get _appService => Get.find();

  @override
  Future<bool> login({required String email, required String password}) async {
    await Future.delayed(2.seconds);
    return email == 'hello@example.com' && password == '123456';
    //TODO
    // final res = await _appService.post('/login', data: data);
    // return res.isSuccess;
  }
}
