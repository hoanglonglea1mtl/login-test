import 'dart:core';

class ValidatorUtils {
  static const msgInvalidEmail = 'Invalid email';

  static const msgPassEmpty = 'Password not empty';

  static const msgPassLength = 'Password not less than 6 characters';

  static String? validateEmpty(
    String? input, {
    String? fieldName,
    String? overrideErrorText,
  }) {
    if (input?.trim().isNotEmpty == true) {
      return null;
    }
    return overrideErrorText ?? 'Please enter ${fieldName ?? 'Field'}';
  }

  static String? validateEmail(String? input) {
    if (input == null || input.isEmpty) {
      return validateEmpty(input, overrideErrorText: msgInvalidEmail);
    }
    final emailRegExp = RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
    if (emailRegExp.hasMatch(input)) {
      return null;
    }
    return msgInvalidEmail;
  }

  static String? validatePassword(String? input) {
    if (input == null || input.isEmpty) {
      return validateEmpty(input, overrideErrorText: msgPassEmpty);
    }
    if (input.length >= 6) {
      return null;
    }
    return msgPassLength;
  }
}
