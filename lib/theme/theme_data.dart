import 'package:flutter/material.dart';
import 'theme.dart';

class AirTalkThemeData {
  // static final InputBorder _inputBorder = OutlineInputBorder(
  //   borderSide: const BorderSide(color: AppColors.colorGreyF6F7FB),
  //   borderRadius: BorderRadius.circular(15),
  // );
  static final themeData = ThemeData(
    brightness: Brightness.light,
    splashColor: Colors.transparent,
    primaryColor: AppColors.primary,
    appBarTheme: AppBarTheme(
      iconTheme: const IconThemeData(color: AppColors.primary, size: 25),
      elevation: 0,
      centerTitle: true,
      color: _colorScheme.background,
    ),
    canvasColor: _colorScheme.background,
    toggleableActiveColor: _colorScheme.primary,
    indicatorColor: _colorScheme.onPrimary,
    bottomAppBarColor: Colors.white,
    scaffoldBackgroundColor: _colorScheme.background,
    snackBarTheme: const SnackBarThemeData(
      behavior: SnackBarBehavior.floating,
    ),
    textTheme: _textTheme,
    // inputDecorationTheme: InputDecorationTheme(
    //   errorMaxLines: 2,
    //   helperMaxLines: 2,
    //   isDense: true,
    //   filled: true,
    //   fillColor: AppColors.colorGreyFFFFFF,
    //   labelStyle: _textTheme.bodyText1!.medium.textGreyBCC0C2,
    //   hintStyle: _textTheme.bodyText1!.medium.textGreyBCC0C2,
    //   focusedBorder: _inputBorder,
    //   border: _inputBorder,
    //   enabledBorder: _inputBorder,
    //   errorBorder: _inputBorder.copyWith(
    //       borderSide: BorderSide(
    //     color: _colorScheme.error,
    //   )),
    //   focusedErrorBorder: _inputBorder.copyWith(
    //     borderSide: BorderSide(
    //       color: _colorScheme.error,
    //     ),
    //   ),
    //   disabledBorder: _inputBorder,
    //   contentPadding: const EdgeInsets.symmetric(
    //     horizontal: 17,
    //     vertical: 15,
    //   ),
    // ),
    cardTheme: CardTheme(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 15,
      shadowColor: Colors.black.withOpacity(0.1),
      color: Colors.white,
    ),
    dividerTheme: DividerThemeData(space: 24, color: Colors.grey.shade200, thickness: 1),
    buttonTheme: ButtonThemeData(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        padding: EdgeInsets.zero,
        disabledColor: Colors.grey.shade400),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        foregroundColor: Colors.white,
        backgroundColor: AppColors.primary,
        elevation: 3,
        disabledForegroundColor: Colors.grey.shade400,
        disabledBackgroundColor: Colors.grey.shade200,
        minimumSize: const Size(100.0, 55.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
      ),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        minimumSize: const Size(100, 55),
        side: const BorderSide(width: 1.5, color: AppColors.primary),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        disabledForegroundColor: Colors.grey.shade400,
      ),
    ),
    colorScheme: _colorScheme,
  );

  static const _regular = FontWeight.w400;
  static const _semiBold = FontWeight.w600;
  static const _bold = FontWeight.w700;

  static const _colorScheme = ColorScheme(
    primary: AppColors.primary,
    secondary: AppColors.primary,
    background: Colors.white,
    onBackground: Colors.black,
    surface: Colors.white,
    onSurface: AppColors.primary,
    error: Colors.red,
    onError: Colors.white,
    onPrimary: Colors.white,
    onSecondary: Colors.white,
    brightness: Brightness.light,
  );

  static const _textTheme = TextTheme(
    headline2: TextStyle(fontWeight: _bold, fontSize: 40.0, height: 50 / 40),
    headline3: TextStyle(fontWeight: _bold, fontSize: 30.0, height: 40 / 30),
    headline4: TextStyle(fontWeight: _bold, fontSize: 24.0, height: 30.0 / 24.0),
    headline5: TextStyle(fontWeight: _bold, fontSize: 20.0, height: 27.0 / 20.0),
    headline6: TextStyle(fontWeight: _bold, fontSize: 18.0, height: 26.0 / 18.0),
    subtitle1: TextStyle(fontWeight: _regular, fontSize: 16.0, height: 23.0 / 16.0),
    subtitle2: TextStyle(fontWeight: _semiBold, fontSize: 14.0, height: 22.0 / 14.0),
    bodyText1: TextStyle(fontWeight: _bold, fontSize: 16.0, height: 23.0 / 16.0),
    bodyText2: TextStyle(fontWeight: _regular, fontSize: 14.0, height: 22.0 / 14.0),
    button: TextStyle(fontWeight: FontWeight.w600, fontSize: 16.0, height: 18.0 / 16.0),
    caption: TextStyle(fontWeight: _regular, fontSize: 12.0, height: 20.0 / 12.0),
    overline: TextStyle(fontWeight: _regular, fontSize: 10.0, height: 15.0 / 10.0),
  );
}
