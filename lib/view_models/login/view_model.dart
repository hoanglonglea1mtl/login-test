import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:login_test/data/data.dart';

class LoginViewModel extends GetxController {
  IAuthRepository get _authRepo => Get.find();

  final formKey = GlobalKey<FormState>();

  final emailTextCtrl = TextEditingController();

  final passTextCtrl = TextEditingController();

  final emailFocus = FocusNode();

  final passwordFocus = FocusNode();

  void onLogin() async {
    if (!formKey.currentState!.validate()) return;
    EasyLoading.show();
    final res = await _authRepo.login(email: emailTextCtrl.text, password: passTextCtrl.text);
    EasyLoading.dismiss();
    if (res) {
      EasyLoading.showSuccess('Login success!');
    }else{
      EasyLoading.showError('Email or password is incorrect');
    }
  }

  @override
  void dispose() {
    emailFocus.dispose();
    passwordFocus.dispose();
    emailTextCtrl.dispose();
    passTextCtrl.dispose();
    super.dispose();
  }
}
