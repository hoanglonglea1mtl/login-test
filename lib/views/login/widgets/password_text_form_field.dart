import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PasswordTextFormField extends StatelessWidget {
  PasswordTextFormField({
    Key? key,
    this.controller,
    this.validator,
    this.label,
    this.hinText,
    this.onChanged,
    this.focusNode, this.onFieldSubmitted,
  }) : super(key: key);
  final TextEditingController? controller;
  final FormFieldValidator<String>? validator;
  final String? label;
  final String? hinText;
  final FocusNode? focusNode;
  final Function(String)? onChanged;
  final ValueChanged<String>? onFieldSubmitted;

  final _obscureText = true.obs;

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return TextFormField(
        controller: controller,
        obscureText: _obscureText.value,
        validator: validator,
        onChanged: onChanged,
        focusNode: focusNode,
        onFieldSubmitted: onFieldSubmitted,
        decoration: InputDecoration(
          labelText: label,
          hintText: hinText,
          suffixIcon: InkWell(
            onTap: _obscureText.toggle,
            child: Icon(
              _obscureText.isTrue ? Icons.visibility_outlined : Icons.visibility_off_outlined,
              size: 26,
              color: Colors.grey.shade400,
            ),
          ),
        ),
      );
    });
  }
}
