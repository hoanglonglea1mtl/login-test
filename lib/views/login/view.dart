import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:login_test/view_models/view_models.dart';
import '../../utils/utils.dart';
import 'widgets/widgets.dart';

class LoginView extends GetView<LoginViewModel> {
  LoginView({Key? key}) : super(key: key);

  final _textTheme = Get.textTheme;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 25),
        children: [
          const SizedBox(height: 200),
          Text(
            'Login',
            style: _textTheme.headline4,
          ),
          const SizedBox(height: 40),
          _buildForm(),
          const SizedBox(height: 100),
          ElevatedButton(
            onPressed: () {
              FocusScope.of(context).unfocus();
              controller.onLogin();
            },
            child: const Text('Login'),
          )
        ],
      ),
    );
  }

  Widget _buildForm() {
    return Form(
      key: controller.formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextFormField(
            controller: controller.emailTextCtrl,
            keyboardType: TextInputType.emailAddress,
            focusNode: controller.emailFocus,
            decoration: const InputDecoration(hintText: 'Email'),
            onFieldSubmitted: (_) => controller.passwordFocus.requestFocus(),
            validator: ValidatorUtils.validateEmail,
          ),
          const SizedBox(height: 30),
          PasswordTextFormField(
            hinText: 'Password',
            controller: controller.passTextCtrl,
            focusNode: controller.passwordFocus,
            validator: ValidatorUtils.validatePassword,
            onFieldSubmitted: (_) => controller.onLogin(),
          ),
        ],
      ),
    );
  }
}
