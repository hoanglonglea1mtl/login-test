import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:login_test/data/data.dart';
import 'package:login_test/utils/utils.dart';

void main() {
  ///test validator
  test('Email invalid test', () {
    final result = ValidatorUtils.validateEmail('');
    expect(result, ValidatorUtils.msgInvalidEmail);
  });

  test('Password empty test', () {
    final result = ValidatorUtils.validatePassword('');
    expect(result, ValidatorUtils.msgPassEmpty);
  });

  test('Password not less than 6 characters test', () {
    final result = ValidatorUtils.validatePassword('12345');
    expect(result, ValidatorUtils.msgPassLength);
  });

  test('Email valid test', () {
    final result = ValidatorUtils.validateEmail('test@gmail.com');
    expect(result, null);
  });

  test('Password valid test', () {
    final result = ValidatorUtils.validatePassword('123456');
    expect(result, null);
  });

  ///test logic
  test('Login success test', () async {
    IAuthRepository authRepo = Get.put(AuthRepository());
    final res = await authRepo.login(email: 'hello@example.com', password: '123456');
    expect(res, true);
  });

  test('Login fail test', () async {
    IAuthRepository authRepo = Get.put(AuthRepository());
    final res = await authRepo.login(email: 'hello@gmail.com', password: '123456');
    expect(res, false);
  });
}
