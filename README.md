# Login Test

### Info

- Flutter version : `3.3.1`
- Run unit test: `flutter test`

### Account

- Login success : 
   - email: `hello@example.com`  
   - password: `123456`

### Link

- [List times for each features](https://docs.google.com/spreadsheets/d/1NNS_6-01OmcZaxDIHDIJRgldpSmWKagsaCQyELIoO30/edit?usp=sharing)
- [File APK](https://drive.google.com/file/d/1vLeW2XWw325iXPXybdhr5xWO5O06E1Fc/view?usp=sharing)